package com.trash.areafigurasgeometricas;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.trash.areafigurasgeometricas.Modelos.Cuadrado;

import androidx.appcompat.app.AppCompatActivity;


public class CuadradoActivity extends AppCompatActivity {

    Button btnCalcular, btnRegresar;
    EditText txtLado;
    TextView txVResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuadrado);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtLado = findViewById(R.id.txtLado);
        txVResultado = findViewById(R.id.txtVResultado);


        final AlertDialog.Builder[] dialg = new AlertDialog.Builder[1];

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btnCalcular.getText().toString().equals("Limpiar")) {
                    txtLado.setText("");
                    txVResultado.setText("");
                    txtLado.requestFocus();
                    btnCalcular.setText("Calcular");
                } else {

                    if (txtLado.getText().toString().length() == 0) {

                        dialg[0] = new AlertDialog.Builder(CuadradoActivity.this);
                        dialg[0].setTitle("Error");
                        dialg[0].setMessage("Ingrese el Lado del Cuadrado");
                        dialg[0].setCancelable(false);
                        dialg[0].setPositiveButton("Entiendo", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogo, int id) {
                                dialg[0].setCancelable(true);
                                txtLado.requestFocus();
                            }
                        });
                        dialg[0].show();
                    } else {

                        double lado = Double.parseDouble(txtLado.getText().toString());
                        Cuadrado cuadrado = new Cuadrado(lado);
                        cuadrado.area();
                        txVResultado.setText(String.valueOf(cuadrado.getAreaCuadrado()));
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CuadradoActivity.this, MainActivity.class);
                startActivity(i);
            }
        });
    }
}