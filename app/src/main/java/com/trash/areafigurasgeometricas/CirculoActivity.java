package com.trash.areafigurasgeometricas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.trash.areafigurasgeometricas.Modelos.Circulo;
import com.trash.areafigurasgeometricas.Modelos.Cuadrado;

public class CirculoActivity extends AppCompatActivity {

    Button btnCalcular, btnRegresar;
    EditText txtRadio;
    TextView txVResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circulo);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtRadio = findViewById(R.id.txtRadio);
        txVResultado = findViewById(R.id.txtVResultado);

        final AlertDialog.Builder[] dialg = new AlertDialog.Builder[1];

        btnCalcular.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                if (btnCalcular.getText().toString().equals("Limpiar")) {
                    txtRadio.setText("");
                    txVResultado.setText("");
                    txtRadio.requestFocus();
                    btnCalcular.setText("Calcular");
                } else {

                    if (txtRadio.getText().toString().length() == 0) {

                        dialg[0] = new AlertDialog.Builder(CirculoActivity.this);
                        dialg[0].setTitle("Error");
                        dialg[0].setMessage("Ingrese el Radio del Ciculo");
                        dialg[0].setCancelable(false);
                        dialg[0].setPositiveButton("Entiendo", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogo, int id) {
                                dialg[0].setCancelable(true);
                                txtRadio.requestFocus();
                            }
                        });
                        dialg[0].show();
                    } else {

                        double radio = Double.parseDouble(txtRadio.getText().toString());
                        Circulo circulo = new Circulo(radio);
                        circulo.area();
                        txVResultado.setText(String.valueOf(circulo.getAreaRadio()));
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CirculoActivity.this, MainActivity.class);
                startActivity(i);
            }
        });
    }
}