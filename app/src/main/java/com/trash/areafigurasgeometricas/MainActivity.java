package com.trash.areafigurasgeometricas;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AlertDialogLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    AlertDialog.Builder dialog;

    private Button btnNext;
    private RadioGroup radioGroup;
    private RadioButton opCuadrado; /*opRectangulo, opTriangulo, opCirculo;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnNext = findViewById(R.id.btnCheck);
        radioGroup = findViewById(R.id.rbtGroup);
        opCuadrado = findViewById(R.id.rbtCuadrado);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.rbtCuadrado:
                        Intent cuad = new Intent(MainActivity.this, CuadradoActivity.class);
                        startActivity(cuad);
                        break;
                    case R.id.rbtRectangulo:
                        Intent rect = new Intent(MainActivity.this, RectanguloActivity.class);
                        startActivity(rect);
                        break;
                    case R.id.rbtTriangulo:
                        Intent trian = new Intent(MainActivity.this, TrianguloActivity.class);
                        startActivity(trian);
                        break;
                    case R.id.rbtCirculo:
                        Intent circ = new Intent(MainActivity.this, CirculoActivity.class);
                        startActivity(circ);
                        break;
                    default:
                        dialog = new AlertDialog.Builder(MainActivity.this);
                        dialog.setTitle("Error");
                        dialog.setMessage("Seleccione una opcion");
                        dialog.setCancelable(false);
                        dialog.setPositiveButton("Vale", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                opCuadrado.requestFocus();
                            }
                        });
                        dialog.show();
                        break;
                }
            }
        });

    }
}