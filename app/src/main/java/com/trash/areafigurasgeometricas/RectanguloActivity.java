package com.trash.areafigurasgeometricas;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.trash.areafigurasgeometricas.Modelos.Rectangulo;

import androidx.appcompat.app.AppCompatActivity;

public class RectanguloActivity extends AppCompatActivity {

    Button btnCalcular, btnRegresar;
    EditText txtBase, txtAltura;
    TextView txVResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtBase = findViewById(R.id.txtBase);
        txtAltura = findViewById(R.id.txtAltura);
        txVResultado = findViewById(R.id.txtVResultado);

        final AlertDialog.Builder[] dialg = new AlertDialog.Builder[1];

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnCalcular.getText().toString().equals("Limpiar")) {
                    txtBase.setText("");
                    txtAltura.setText("");
                    txVResultado.setText("");
                    txtBase.requestFocus();
                    btnCalcular.setText("Calcular");

                } else {

                    if (txtBase.getText().toString().length() == 0) {

                        dialg[0] = new AlertDialog.Builder(RectanguloActivity.this);
                        dialg[0].setTitle("Error");
                        dialg[0].setMessage("Ingrese la base del Rectangulo");
                        dialg[0].setCancelable(false);
                        dialg[0].setPositiveButton("Entiendo", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogo, int id) {
                                dialg[0].setCancelable(true);
                                txtBase.requestFocus();
                            }
                        });
                        dialg[0].show();
                    } else {
                        if (txtAltura.getText().toString().length() == 0) {

                            dialg[0] = new AlertDialog.Builder(RectanguloActivity.this);
                            dialg[0].setTitle("Error");
                            dialg[0].setMessage("Ingrese la altura del Rectangulo");
                            dialg[0].setCancelable(false);
                            dialg[0].setPositiveButton("Entiendo", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogo, int id) {
                                    dialg[0].setCancelable(true);
                                    txtAltura.requestFocus();
                                }
                            });
                            dialg[0].show();
                        } else {
                            double base = Double.parseDouble(txtBase.getText().toString());
                            double altura = Double.parseDouble(txtAltura.getText().toString());
                            Rectangulo rectangulo = new Rectangulo(base, altura);
                            rectangulo.area();
                            txVResultado.setText(String.valueOf(rectangulo.getAreaRectangulo()));
                        }
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RectanguloActivity.this, MainActivity.class);
                startActivity(i);
            }
        });
    }
}